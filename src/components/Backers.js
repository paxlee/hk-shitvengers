import React from 'react';
import styled from 'styled-components';

import withFundings from './withFundings';
import Currency from './Currency';

const Backers = ({ fundings }) => (
  <div>
    {fundings.length ? (
      <div>
        <h2>籌屎者</h2>
        <ul>
          {fundings.map(funding => (
            <li key={funding.id}>
              {funding.username ? funding.username : '無名屎'} - {' '}
              {funding.amount}嚿
            </li>
          ))}
        </ul>
      </div>
    ) : null}
  </div>
);

export default withFundings(Backers);
